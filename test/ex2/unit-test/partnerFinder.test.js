"use strict";
const partnerFinder = require("../../../src/ex2/partnerFinder");
const partnerService = require("../../../src/ex2/services/partnerService");
const fs = require("fs");

const londonCoords = { lat: 51.515419, lon: -0.141099 };
const closePartnerFirst = {
    "id": 1,
    "organization": "Definitely not a fake british coorp",
    "offices": [
      {
        "location": "London, UK",
        "address": "Piccadilly Circus 32",
        "coordinates": "51.115419,-0.171099"
      }
    ]
};
const closePartnerSecond = {
    "id": 2,
    "organization": "Not a fake british coorp",
    "offices": [
      {
        "location": "London, UK",
        "address": "Piccadilly Circus 31",
        "coordinates": "51.135419,-0.161099"
      }
    ]
};
const farPartner = {
    "id": 3,
    "organization": "Balance at Work",
    "offices": [
      {
        "location": "Sydney, Australia",
        "address": "Suite 1308, 109 Pitt St \nSydney 2000",
        "coordinates": "-33.8934219,151.20404600000006"
      }
    ]
};
const bothPartner = {
    "id": 4,
    "organization": "Blue Square 360",
    "offices": [
      {
        "location": "Singapore",
        "address": "Ocean Financial Centre, Level 40, 10 Collyer Quay, Singapore, 049315",
        "coordinates": "1.28304,103.85199319999992"
      },
      {
        "location": "London, UK",
        "address": "St Saviours Wharf, London SE1 2BE",
        "coordinates": "51.5014767,-0.0713608999999451"
      }
    ]
};
let dropables;


beforeEach(function () {
    dropables = sinon.collection;
});

afterEach(function () {
    dropables.restore();
});

describe("partnerFinder", () => {


    describe("getPartnersOnRadius", () => {

        it("should fail when origin has incorrect format", () => {
            return partnerFinder.getPartnersOnRadius(100, { x: 1, y: 2 }).then( partners => {
                assert(false, "Should return an error.");
            }, err => {
                assert(true, "Should return an error.");
            });
            
        });

         it("should return an error if no partners can be read by partnerService", () => {
            dropables.stub(partnerService, "getPartners").rejects("generic error");

            return partnerFinder.getPartnersOnRadius(100, londonCoords).then( partners => {
                assert(false, "Should return an error.");
            }, err => {
                expect(partnerService.getPartners).to.have.been.calledOnce;
                expect(err.name).to.be.equal("generic error");
            });
        });

        it("should return an empty array if there are no partners within the radius", () => {
            dropables.stub(partnerService, "getPartners").resolves([farPartner]);

            return partnerFinder.getPartnersOnRadius(1, londonCoords).then( partners => {
                expect(partnerService.getPartners).to.have.been.calledOnce;
                expect(partners).to.be.an("array");
                expect(partners).to.be.empty;
            }, err => {
                assert(false, "Should not return an error.");
            });
        });

        it("should return name and addresses of partners within radius ordered ascendently by name", () => {
            dropables.stub(partnerService, "getPartners").resolves([farPartner, closePartnerSecond, closePartnerFirst]);

            return partnerFinder.getPartnersOnRadius(100, londonCoords).then( partners => {
                expect(partnerService.getPartners).to.have.been.calledOnce;
                expect(partners).to.be.an("array");
                expect(partners).to.have.lengthOf(2);
                partners.forEach((partner,index) => {
                    if (index > 0) assert(partner.organization > partners[index-1].organization, "Result is not ordered ascendently by name");
                });
                expect(partners[0]).to.not.have.any.keys("id");
                expect(partners[0]).to.include({ organization: closePartnerFirst.organization })
                expect(partners[0].addresses).to.be.an("array");
                expect(partners[0].addresses[0]).to.be.an("string").equal(closePartnerFirst.offices[0].address);
            }, err => {
                assert(false, "Should not return an error.");
            });
        });

        it("should return all offices, not only those offices within the radius", () => {
            dropables.stub(partnerService, "getPartners").resolves([farPartner, bothPartner]);

            return partnerFinder.getPartnersOnRadius(100, londonCoords).then( partners => {
                expect(partnerService.getPartners).to.have.been.calledOnce;
                expect(partners).to.be.an("array");
                expect(partners).to.have.lengthOf(1);
                expect(partners[0]).to.not.have.any.keys("id");
                expect(partners[0]).to.include({ organization: bothPartner.organization })
                expect(partners[0].addresses).to.be.an("array");
                expect(partners[0].addresses).to.have.lengthOf(bothPartner.offices.length);
                bothPartner.offices.forEach( office => {
                    expect(partners[0].addresses).to.include(office.address);
                });
            }, err => {
                assert(false, "Should not return an error.");
            });
        });

    })

})