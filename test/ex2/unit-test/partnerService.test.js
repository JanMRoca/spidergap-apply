"use strict";
const PartnerService = require("../../../src/ex2/services/partnerService");
const fs = require("fs");

let dropables;
const partnerJson = [
  {
    "id": 1,
    "urlName": "balance-at-work",
    "organization": "Balance at Work",
    "customerLocations": "across Australia, Pacific and Oceania",
    "willWorkRemotely": true,
    "website": "http://www.balanceatwork.com.au/",
    "services": "At Balance at Work, we want to help you make work a joy for your employees and you! We specialize in leadership development, talent management and career coaching, and use Spidergap as one of our tools to help employees focus their development and achieve more.",
    "offices": [
      {
        "location": "Sydney, Australia",
        "address": "Suite 1308, 109 Pitt St \nSydney 2000",
        "coordinates": "-33.8934219,151.20404600000006"
      }
    ]
  }
];


beforeEach(function () {
    dropables = sinon.collection;
});

afterEach(function () {
    dropables.restore();
});

describe("partnerService", () => {

    describe("getPartners", () => {

        it("should fail when there is no readable json", () => {
            dropables.stub(fs, "readFile").yields("generic error", null);

            return PartnerService.getPartners().then(
                (partners) => {
                    assert(false, "Should return an error.");
                }, (err) => {
                    assert(true, "Should return an error.");
                }
            )
            
        });

        it("should fail when the json is incorrect", () => {
            dropables.stub(JSON, "parse").throws();

            return PartnerService.getPartners().then(
                (partners) => {
                    assert(false, "Should return an error.");
                }, (err) => {
                    assert(true, "Should return an error.");
                }
            )      
        });

        it("should return an array of partners", () => {
            dropables.stub(fs, "readFile").yields(null, JSON.stringify(partnerJson));

            return PartnerService.getPartners().then(
                (partners) => {
                    expect(fs.readFile).to.have.been.calledOnce;
                    expect(partners).to.be.an("array");
                    expect(partners[0]).to.be.an("object").
                        that.has.all.keys("id", "urlName", "organization", "customerLocations", "willWorkRemotely", "website", "services", "offices");
                    expect(partners[0].offices).to.be.an("array");
                    expect(partners[0].offices[0]).to.be.an("object").that.has.all.keys("location", "address", "coordinates");
                }, (err) => {
                    assert(false, "Should not return an error.");
                }
            )
        });

    })

})