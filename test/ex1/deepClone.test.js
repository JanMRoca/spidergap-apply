"use strict";
const deepCloner = require("../../src/ex1/deepClone").deepClone;


describe("deepClone", () => {
    
    it("should clone primitive properties", () => {
        const obj = { a: "1", b: 2, c: true };
        const clone = deepCloner(obj);

        expect(obj).to.deep.equal(clone);
    });

    it("should clone array properties", () => {
        const obj = { a: [ [1,2,3], [4,5,6], ]};
        const clone = deepCloner(obj);

        expect(obj).to.deep.equal(clone);

        obj["a"][0][0] = 2;
        expect(obj).to.not.deep.equal(clone);
    });

    it("should clone object properties", () => {
        const obj = {name: "Paddy", address: {town: "Lerum", country: "Sweden"}};
        const clone = deepCloner(obj);

        expect(obj).to.deep.equal(clone);

        obj["address"]["town"] = "Lorem";
        expect(obj).to.not.deep.equal(clone);
    });

    it("should clone functions", () => {
        const functReturn = Math.random();
        const obj = { a: '1', b : function () { return functReturn } };
        const clone = deepCloner(obj);

        expect(obj).to.deep.equal(clone);
        expect(obj.b).to.equal(clone.b);
    })

    it("should not crash when a parameter is undefined", () => {
        const obj = {name: "Paddy", address: undefined };
        const clone = deepCloner(obj);

        expect(obj).to.deep.equal(clone);
    });

    it("should not crash when the cloned object is undefined", () => {
        const obj = undefined;
        const clone = deepCloner(obj);
    });

})