"use strict";
const partnerService = require("./services/partnerService");

const EARTH_RADIUS = 6371;


function toRads(degrees) {
    return degrees * Math.PI / 180.0;
}

function toDegreeCoords(coordString) {
    const coordArray = coordString.split(",");
    return { lat: coordArray[0], lon: coordArray[1] }; 
}

function degreeCoordsToRads(coords) {
    return { lat: toRads(coords.lat), lon: toRads(coords.lon) };
}

function getCoordsDistanceInKm(orig, dest) {
    const origRads = degreeCoordsToRads(orig);
    const destRads = degreeCoordsToRads(dest);

    const centralAngle = Math.acos( Math.sin(origRads.lat) * Math.sin(destRads.lat) + 
        Math.cos(origRads.lat) * Math.cos(destRads.lat) * Math.cos(origRads.lon - destRads.lon) );

    return EARTH_RADIUS * centralAngle;
}

function getPartnersOnRadius(radiusKm, origin) {
    return new Promise(function (resolve, reject) {

        if (!origin.lat|| !origin.lon) return reject("Origin argument must be an object with form { lat, lon }");

        partnerService.getPartners().then( partners => {
            let result = partners.filter(partner => {
                 return partner.offices.some(office => {
                     return getCoordsDistanceInKm(origin, toDegreeCoords(office.coordinates)) <= radiusKm 
                 })
            }).map(partner => {
                let mappedPartner = {}

                mappedPartner.organization = partner.organization;
                mappedPartner.addresses = partner.offices.map(office => { return office.address });
                
                return mappedPartner;
            })
            .sort( (partner1, partner2) => {
                return partner1.organization.localeCompare(partner2.organization);
            });

            resolve(result);
        }, err => {
            reject(err);
        });
    });
}

module.exports = {
    getPartnersOnRadius: getPartnersOnRadius
}
