"use strict";
const partnerFinder = require("./partnerFinder");
const londonCoords = { lat: 51.515419, lon: -0.141099 };
const desiredRadius = 100;


partnerFinder.getPartnersOnRadius(desiredRadius, londonCoords).then( partners => {
    console.log(partners);
}, err => {
    console.log(`something went wrong, err is ${err}`);
});