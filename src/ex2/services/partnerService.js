"use strict";
const fs = require("fs");
const path = require("path");

const jsonPath = path.join(__dirname, "..", "partners.json");


function getPartners() {
    return new Promise(function (resolve, reject) {

        fs.readFile(jsonPath, (err,data) => {
            if (err) reject(err);

            try {
                resolve(JSON.parse(data));
            } catch (e) {
                reject(e);
            }
        });
    });
}

module.exports = {
    getPartners: getPartners
}