"use strict";


function deepClone(obj) {

    if (obj === undefined) return undefined;
    
    const clone = (obj.constructor === Array) ? [] : {};
    const result = Object.keys(obj).forEach( key => {
        const val = obj[key]

        if (typeof val === "object") clone[key] = deepClone(val);
        else clone[key] = val;
    });
    
    return clone;
}

module.exports = {
    deepClone: deepClone
}